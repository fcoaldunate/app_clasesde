class CreateGradeUsers < ActiveRecord::Migration
  def change
    create_table :grade_users do |t|
      t.belongs_to :grade
      t.belongs_to :user
      t.timestamps
    end
  end
end
