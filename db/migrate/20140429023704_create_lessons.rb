class CreateLessons < ActiveRecord::Migration
  def change
    create_table :lessons do |t|
      t.string :title
      t.belongs_to :grade
      t.text :content
      t.date :date
      t.timestamps
    end
  end
end
