class CreateGrades < ActiveRecord::Migration
  def change
    create_table :grades do |t|
      t.string :name
      t.text :description
      t.belongs_to :category
      t.timestamps
    end
  end
end
