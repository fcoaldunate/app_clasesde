class AppMail < ActionMailer::Base
  default from: "contacto@clasesde.cl"
  layout 'awesome'

  def send_message(message, user, email)
    @user    = user
    @message = message
    mail(to: email, subject: 'Nuevo mensaje de clasesde.cl')
  end
end
