class GradesController < ApplicationController
  before_action :authenticate_user!

  def index
    @grades = current_user.grade_users
  end

  def show
    @grade = current_user.grades.find(params[:id])
  end
end
