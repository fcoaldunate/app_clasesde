class MessagesController < ApplicationController
  before_action :authenticate_user!

  def index
  end

  def create
    @message = current_user.messages.new message_params

    respond_to do |format|
      if @message.save
        AppMail.send_message(@message, current_user, "contacto@clasesde.cl").deliver
        format.html { redirect_to  messages_path}
      else
        format.html { render :index}
      end
    end
  end

  def message_params
    params.require(:message).permit(:message)
  end
end
