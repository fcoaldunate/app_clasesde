class UsersController < ApplicationController
  before_action :authenticate_user!
  
  def account
  end

  def update
    respond_to do |format|
      if current_user.update_attributes(user_params)
        format.html {redirect_to account_users_path}
      else
        format.html {render :account}
      end
    end
  end

  private

  def user_params
    params.require(:user).permit(:email, :first_name, :last_name, :age, :password, :password_confirm)
  end
end
