class GradeUser < ActiveRecord::Base
  belongs_to :user
  belongs_to :grade

  validates_uniqueness_of :grade_id, :scope => :user_id
  # before_save :check_exists
  #
  # def check_exists
  #   not GradeUser.where(user: self.user, grade: self.grade).any?
  # end
end
