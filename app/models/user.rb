class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  has_many :grade_users
  has_many :grades, :through => :grade_users
  has_many :messages, as: :from
  has_many :admin_messages, as: :to, class_name: "Message"

  has_many :user_messages,   as: :from, class_name: "Message"
  # has_many :admin_messages, class_name: "Message", conditions: {"messages.to_id" => "users.id", "messages.to_type" => "User"}
  # has_many :admin_messages, :through => :messages_, source: :to, source_type: "User"


  accepts_nested_attributes_for :grades, :reject_if => :all_blank, :allow_destroy => true
  accepts_nested_attributes_for :grade_users, :reject_if => :all_blank, :allow_destroy => true

  def name
    "#{first_name} #{last_name}" || email
  end

  def messages_
    (user_messages + admin_messages).sort_by{|e| e.created_at}.reverse
  end
end
