class Grade < ActiveRecord::Base
  belongs_to :category
  has_many :grade_users
  has_many :lessons
  has_many :users, :through => :grade_users
end
