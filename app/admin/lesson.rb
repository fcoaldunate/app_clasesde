ActiveAdmin.register Lesson do

  form do |f|
    f.inputs do
      f.input :grade
      f.input :title
      f.input :content, as: :html_editor
    end

    f.actions
  end

  controller do
    def permitted_params
      params.permit lesson: [:grade_id, :title, :content]
    end
  end

end
