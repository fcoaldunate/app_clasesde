ActiveAdmin.register Message do

  form do |f|
    f.inputs do
      f.input :message
      f.input :to, :collection => User.all.map{|u| ["#{u.first_name} #{u.last_name}", u.id]}
    end
    f.actions
  end

  controller do
    def create
      super do |format|
        # Do what you want here ...
        user = User.find params[:message][:to_id]
        @message.to = user
        @message.from = current_admin_user

        if @message.save
          AppMail.send_message(@message, current_admin_user, user.email).deliver
        end
      end
    end

    def permitted_params
      params.permit message: [:message, :to_id, :to_type, :from_id, :from_type]
    end
  end

  # See permitted parameters documentation:
  # https://github.com/gregbell/active_admin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  # permit_params :list, :of, :attributes, :on, :model
  #
  # or
  #
  # permit_params do
  #  permitted = [:permitted, :attributes]
  #  permitted << :other if resource.something?
  #  permitted
  # end

end
