ActiveAdmin.register GradeUser do

  index do
    column :user
    column :grade
    default_actions
  end

  # See permitted parameters documentation:
  # https://github.com/gregbell/active_admin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  controller do
    def permitted_params
      params.permit grade_user: [:grade_id, :user_id]
    end
  end
  #
  # or
  #
  # permit_params do
  #  permitted = [:permitted, :attributes]
  #  permitted << :other if resource.something?
  #  permitted
  # end

end
