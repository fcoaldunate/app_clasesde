ActiveAdmin.register User do

  index do
    column :email
    default_actions
  end

  filter :email

  form do |f|
    f.inputs "User Details" do
      f.input :email
      f.input :first_name
      f.input :last_name
      f.input :age
      f.input :password
      f.input :password_confirmation
    end

    # f.inputs "Grade" do
    #   f.has_many :grades, :allow_destroy => true do |g|
    #     g.input :id
    #   end
    # end
    # f.has_many :grade_users do |k|
    #   if k.object.nil?
    #     k.input :grade, :label => 'Keyword'
    #   else
    #     k.input :grade, :label => k.object
    #     k.input :_destroy, :as => :boolean, :label => "delete"
    #   end
    # end
    f.actions
  end

  controller do
    def permitted_params
      params.permit user: [:first_name, :last_name, :age, :email, :password, :password_confirmation, grade_users_attributes: [:grade_id, :_destroy]]
    end

    def update
      if params[:user][:password].blank?
        params[:user].delete("password")
        params[:user].delete("password_confirmation")
      end
      super
    end

  end

end
